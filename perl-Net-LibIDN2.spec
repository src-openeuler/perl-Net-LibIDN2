Name:           perl-Net-LibIDN2
Version:        1.02
Release:        2
Summary:        GNU Libidn2 connected to Perl
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Net-LibIDN2
Source0:        https://cpan.metacpan.org/authors/id/T/TH/THOR/Net-LibIDN2-%{version}.tar.gz
BuildRequires:  coreutils findutils libidn2-devel >= 2.0.0 perl-interpreter perl-devel perl-generators
BuildRequires:  perl(Devel::CheckLib) perl(ExtUtils::CBuilder) perl(lib) perl(Module::Build) perl(strict)
BuildRequires:  perl(warnings) sed perl(:VERSION) >= 5.6 perl(DynaLoader) perl(Exporter) perl(Encode)
BuildRequires:  perl(Test::More) >= 0.10 glibc-langpack-en perl(POSIX)

%description
Provides bindings for GNU Libdin2, a C Library for handling internationalized domain names based on 
IDNA 2008,Punycode and TR46.

%package_help

%prep
%autosetup -n Net-LibIDN2-%{version} -p1
rm -rf inc
sed -i -e '/^inc\//d' MANIFEST

%build
perl Build.PL installdirs=vendor optimize="$RPM_OPT_FLAGS"
./Build

%install
./Build install destdir=$RPM_BUILD_ROOT create_packlist=0
%{_fixperms} $RPM_BUILD_ROOT/*

%check
./Build test

%files
%license LICENSE
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Net*

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1.02-2
- drop useless perl(:MODULE_COMPAT) requirement

* Sat Sep 09 2023 wangkai <13474090681@163.com> - 1.02-1
- Update package to 1.02

* Tue Jul 20 2021 Xu Jin <jinxu@kylinos.cn> - 1.01-1
- Update package to 1.01

* Mon Mar 2 2020 <wutao61@huawei.com> - 1.00-7
- Package init
